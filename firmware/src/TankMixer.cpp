

#include <Arduino.h>
#include "TankMixer.h"
#include "CONSTANTS.h"


//https://home.kendra.com/mauser/joystick.html

void TankMixer::loop() {
    signed char speed = joystickInout->speed();
    int y = map(speed, SIGNED_CHAR_MIN, SIGNED_CHAR_MAX, -100, 100);
    signed char steer = joystickInout->steer();
    int x = -map(steer, SIGNED_CHAR_MIN, SIGNED_CHAR_MAX, -100, 100);

    //Calculate R+L (Call it V): V =(100-ABS(X)) * (Y/100) + Y
    int v = (100-abs(x)) * (y/100) + y;
    //Calculate R-L (Call it W): W= (100-ABS(Y)) * (X/100) + X
    int w = (100-abs(y)) * (x/100) + x;
    //Calculate R: R = (V+W) /2
    //Calculate L: L= (V-W)/2
    this->r = (v+w)/2;
    this->l = (v-w)/2;
    //Serial.printf("mixing x(%i):%i y(%i):%i left:%i right:%i \n", steer , x, speed, y, l, r);
}

signed char TankMixer::left() {
    return l;
}

signed char TankMixer::right() {
    return r;
}
