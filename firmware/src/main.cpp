
#include "Arduino.h"
#include "BLEDevice.h"
#include "BleScanner.h"
#include "Ble.h"
#include "TankMixer.h"
#include <Servo.h>
#include <Button.h>
#include <FastLED.h>
#include <VirtualLed.h>

#define bleServerName "Botfi"
#define B1_PIN 35
#define B2_PIN 23

#define AIN1_PIN 32
#define AIN2_PIN 33
#define BIN1_PIN 26
#define BIN2_PIN 25
#define CIN1_PIN 13
#define CIN2_PIN 12

#define DRIVER_NSLEEP_PIN 14
#define DRIVER_FAULT_PIN 24

#define DRIVER2_NSLEEP_PIN 19
#define DRIVER2_FAULT_PIN 18

#define NUM_LEDS 2
#define LED_PIN 5

#define WEAPON_MOVE_DURATION 1000


TaskHandle_t connectionLoopHandle;

CRGB colors[] = {RED, BLUE, GREEN};
int led1Index = 0;
int led2Index = 1;

Servo ain1Servo;
Servo ain2Servo;
Servo bin1Servo;
Servo bin2Servo;
Servo cin1Servo;
Servo cin2Servo;


Button b1(B1_PIN);
Button b2(B2_PIN);

int motorADirection = 1;
int motorBDirection = -1;
int motorCDirection = 1;

BLEUUID GATT_hid("00001812-0000-1000-8000-00805f9b34fb");
BleScanner scanner(GATT_hid);
Ble *device;
TankMixer *mixer;

boolean slow = true;

CRGB LED[NUM_LEDS];

bool m1On = false;
bool m2On = false;

int ain1 = 0;
int ain2 = 0;
int bin1 = 0;
int bin2 = 0;

long lastLog = millis();


void deviceConnectionLoop(void *noParam) {
    for (;;) {
        if (!device) {
            std::optional<BLEAdvertisedDevice> scanned = scanner.scan();
            if (scanned) {
                device = new Ble();
                Serial.printf("scan found device : %s\n", scanned.value().toString().c_str());
                device->connect(scanned.value().getAddress(), GATT_hid);
                mixer = new TankMixer(device);
            } else {
                delay(1000);
            }
        } else {
            delay(5000);
        }
    }
}

void applyLeds() {
    LED[0] = colors[led1Index];
    LED[1] = colors[led2Index];
    FastLED.show();
}

void setup() {


    Serial.begin(115200);
    BLEDevice::init(bleServerName);
    scanner.setup();


    ain1Servo.attach(AIN1_PIN, -1, 0, 180, 0, 8000);
    ain2Servo.attach(AIN2_PIN, -1, 0, 180, 0, 8000);

    bin1Servo.attach(BIN1_PIN, -1, 0, 180, 0, 8000);
    bin2Servo.attach(BIN2_PIN, -1, 0, 180, 0, 8000);

    cin1Servo.attach(CIN1_PIN, -1, 0, 180, 0, 8000);
    cin2Servo.attach(CIN2_PIN, -1, 0, 180, 0, 8000);

    pinMode(DRIVER_FAULT_PIN, INPUT);
    pinMode(DRIVER_NSLEEP_PIN, OUTPUT);
    digitalWrite(DRIVER_NSLEEP_PIN, HIGH);

    pinMode(DRIVER2_FAULT_PIN, INPUT);
    pinMode(DRIVER2_NSLEEP_PIN, OUTPUT);
    digitalWrite(DRIVER2_NSLEEP_PIN, HIGH);


    b1.setup();
    b2.setup();


  //  pinMode(LED_PIN, OUTPUT);

    //FastLED.setBrightness(10);
    //FastLED.addLeds<WS2812B, LED_PIN, GRB>(LED, NUM_LEDS);
    delay(2000);
    //applyLeds();

    xTaskCreatePinnedToCore(&deviceConnectionLoop, "deviceConnection", 10000, NULL, 5000, &connectionLoopHandle, 0);
}


void log() {
    int fault = digitalRead(DRIVER_FAULT_PIN);
    //Serial.printf("test motor (fault:%i) %i A1:%i A2:%i B1:%i B2:%i \n", fault, motorCurrentState, ain1, ain2, bin1, bin2);
}

void loop() {

    if (device) {
        if (mixer) {
            mixer->loop();
            signed char left = motorADirection * ((slow) ? 0.5 : 1) * mixer->left();
            signed char right = motorBDirection * ((slow) ? 0.5 : 1) * mixer->right();
            if (left > 0) {
                ain1Servo.write(map(left, 0, 100, 0, 180));
                ain2Servo.write(0);
            } else {
                ain1Servo.write(0);
                ain2Servo.write(map(-left, 0, 100, 0, 180));
            }
            if (right > 0) {
                bin1Servo.write(map(right, 0, 100, 0, 180));
                bin2Servo.write(0);
            } else {
                bin1Servo.write(0);
                bin2Servo.write(map(-right, 0, 100, 0, 180));
            }
        }
    }

    if (device && device->lastTriggerChange != 0) {
        if (device->triggerStatus) {
            cin1Servo.write(90);
            cin2Servo.write(0);
        } else if (millis() - device->lastTriggerChange < WEAPON_MOVE_DURATION) {
            cin1Servo.write(0);
            cin2Servo.write(90);
        } else {
            cin1Servo.write(0);
            cin2Servo.write(0);
        }
    }

    if (b1.popClicked()) {
        Serial.println("B1 pressed");
        slow = !slow;
        led1Index = (led1Index + 1) % sizeof(colors);
    //    applyLeds();
    }

    if (b2.popClicked()) {
        Serial.println("B2 pressed");
        led2Index = (led2Index + 1) % sizeof(colors);
        //  applyLeds();
    }
    if ((millis() - lastLog) > 500) {
        lastLog = millis();
        led1Index = !led1Index;
//       digitalWrite(LED_PIN, led1Index);
        log();
    }
}
