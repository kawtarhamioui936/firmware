

#pragma once


#include "JoystickInput.h"
#include "TankMotorSpeed.h"

class TankMixer : public TankMotorSpeed{
private:
    JoystickInput *joystickInout;
public:
    TankMixer(JoystickInput *joystickInput): joystickInout(joystickInput){};
    void loop();
    signed char left() override;
    signed char right() override;

    int l;
    int r;
};



